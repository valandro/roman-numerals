[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Roman numerals

A simple application to convert numerals to roman numbers.

### Running

- **Clean task**: Remove the `target` folder.

```makefile
make clean
```

- **Compile task**: Generate the `.jar` file.

```makefile
make build
```

- **Execute task**: Run the program to convert decimal/roman numbers.

```makefile
make run
```


### Using

After type `make run`, you should see something like that: 

```

-----------------------------------------
WELCOME TO THE ROMAN NUMERALS CONVERTER!!
-----------------------------------------
If you type a decimal then will discover
the roman number.
If you type a roman number then will
discover the decimal number.
-----------------------------------------
Alert: The biggest decimal number allowed
is 3999.
-----------------------------------------
Type a roman number or decimal:
```

Now you can just type **decimal or a roman number** and you'll get the conversion.

### Requirements

- Maven 3.6.2+
- Java JDK 8
- GNU Make

### References

- [Wikipedia - Roman numerals](https://en.wikipedia.org/wiki/Roman_numerals)
- [Quora - The biggest roman numeral](https://www.quora.com/Which-is-the-maximum-number-in-Roman-numerals)

### License

[MIT License](./LICENSE)