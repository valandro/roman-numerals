clean:
	rm -rf target/
	mvn clean resources:resources
build:
	mvn clean package
run:
	java -jar target/roman-numerals-1.0.0-jar-with-dependencies.jar