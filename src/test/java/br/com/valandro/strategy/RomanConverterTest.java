package br.com.valandro.strategy;

import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import br.com.valandro.domain.RomanAlgarism;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RomanConverterTest {

    @InjectMocks
    private RomanConverter romanConverter;

    @Test
    public void shouldReturnFalseWhenInputIsNull() {
        //given
        final String input = null;

        //when
        final Boolean result = romanConverter.verifyStrategy(input);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseWhenRomanNumberIsNotValid() {
        //given
        final RomanAlgarism[] romanAlgarisms = RomanAlgarism.getRepeatableAlgarisms();
        final RomanAlgarism romanAlgarism = romanAlgarisms[nextInt(0, romanAlgarisms.length)];
        final String input = StringUtils.repeat(romanAlgarism.name(), 4);

        //when
        final Boolean result = romanConverter.verifyStrategy(input);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueWhenRomanNumberValid() {
        //given
        final RomanAlgarism[] romanAlgarisms = RomanAlgarism.getRepeatableAlgarisms();
        final RomanAlgarism romanAlgarism = romanAlgarisms[nextInt(0, romanAlgarisms.length)];
        final String input = StringUtils.repeat(romanAlgarism.name(), 3);

        //when
        final Boolean result = romanConverter.verifyStrategy(input);

        //then
        assertTrue(result);
    }

    @Test
    public void shouldConvertEightHundredNinetyFive() {
        //given
        final String input = "DCCCXCV";

        //when
        final String result = romanConverter.convert(input);

        //then
        assertEquals("895", result);
    }

    @Test
    public void shouldConvertThreeThousandNineHundredNinetyNine() {
        //given
        final String input = "MMMCMXCIX";

        //when
        final String result = romanConverter.convert(input);

        //then
        assertEquals("3999", result);
    }
}
