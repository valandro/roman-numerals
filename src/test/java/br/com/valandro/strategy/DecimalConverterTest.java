package br.com.valandro.strategy;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DecimalConverterTest {

    @InjectMocks
    private DecimalConverter decimalConverter;

    @Test
    public void shouldReturnFalseWhenInputIsNull() {
        //given
        final String input = null;

        //when
        final Boolean result = decimalConverter.verifyStrategy(input);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseWhenInputIsNotComposedOnlyWithNumbers() {
        //given
        final String input = randomNumeric(3) + "a";

        //when
        final Boolean result = decimalConverter.verifyStrategy(input);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseWhenInputIsGreaterThenMaximum() {
        //given
        final String input = randomNumeric(7);

        //when
        final Boolean result = decimalConverter.verifyStrategy(input);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueWhenInputIsValid() {
        //given
        final String input = randomNumeric(3);

        //when
        final Boolean result = decimalConverter.verifyStrategy(input);

        //then
        assertTrue(result);
    }

    @Test
    public void shouldReturnOneThousandFourteen() {
        //given
        final String input = "1014";

        //when
        final String result = decimalConverter.convert(input);

        //then
        assertEquals("MXIV", result);
    }

    @Test
    public void shouldReturnFiveHundredEightyFour() {
        //given
        final String input = "584";

        //when
        final String result = decimalConverter.convert(input);

        //then
        assertEquals("DLXXXIV", result);
    }
}
