package br.com.valandro.service;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.support.membermodification.MemberMatcher.field;

import br.com.valandro.strategy.DecimalConverter;
import br.com.valandro.strategy.RomanConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = { ConverterService.class, MessageService.class })
public class ConverterServiceTest {

    @Mock
    private DecimalConverter decimalConverter;
    @Mock
    private RomanConverter romanConverter;

    @Before
    public void setUp() throws Exception {
        whenNew(DecimalConverter.class)
                .withNoArguments().thenReturn(decimalConverter);

        whenNew(RomanConverter.class)
                .withNoArguments().thenReturn(romanConverter);

        PowerMockito.mockStatic(MessageService.class);
    }

    @Test
    public void shouldReturnInvalidWhenInputIsNull() {
        //given
        final String input = null;
        final String invalidInputMessage = randomNumeric(10);

        given(decimalConverter.verifyStrategy(anyString()))
                .willReturn(Boolean.FALSE);

        given(romanConverter.verifyStrategy(anyString()))
                .willReturn(Boolean.FALSE);

        given(MessageService.invalidInput())
                .willReturn(invalidInputMessage);

        //when
        final String result = ConverterService.converter(input);

        //then
        assertEquals(invalidInputMessage, result);
        verify(decimalConverter).verifyStrategy(input);
        verify(romanConverter).verifyStrategy(input);
        verifyStatic(MenuService.class);
    }
}
