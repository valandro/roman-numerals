package br.com.valandro.service;

import java.util.Scanner;

public class MenuService {
    public static void displayMenu() {
        MessageService.displayMenu();
        final Scanner scanner = new Scanner(System.in);
        final String input = scanner.nextLine();
        final String result = ConverterService.converter(input);
        MessageService.printResult(result);
    }
}
