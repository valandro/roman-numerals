package br.com.valandro.service;

public class MessageService {
    public static void displayMenu() {
        System.out.println("-----------------------------------------");
        System.out.println("WELCOME TO THE ROMAN NUMERALS CONVERTER!!");
        System.out.println("-----------------------------------------");
        System.out.println("If you type a decimal then will discover");
        System.out.println("the roman number.");
        System.out.println("If you type a roman number then will");
        System.out.println("discover the decimal number.");
        System.out.println("-----------------------------------------");
        System.out.println("Alert: The biggest decimal number allowed");
        System.out.println("is 3999.");
        System.out.println("-----------------------------------------");
        System.out.println("Type a roman number or decimal:");
    }

    public static String invalidInput() {
        return " Your input isn't a valid decimal either a roman numeral.";
    }

    public static void printResult(String result) {
        System.out.println("-----------------------------------------");
        System.out.println("Result: " + result);
    }
}
