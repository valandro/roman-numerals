package br.com.valandro.service;

import br.com.valandro.strategy.ConverterStrategy;
import br.com.valandro.strategy.DecimalConverter;
import br.com.valandro.strategy.RomanConverter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ConverterService {
    public static String converter(final String input) {
        final List<ConverterStrategy> strategies = Arrays.asList(new DecimalConverter(), new RomanConverter());

        final ConverterStrategy converterStrategy = strategies.stream()
                .filter(strategy -> strategy.verifyStrategy(input))
                .findFirst()
                .orElse(null);

        final String result = Optional.ofNullable(converterStrategy)
                .map(c -> c.convert(input))
                .orElseGet(MessageService::invalidInput);

        return result;
    }
}
