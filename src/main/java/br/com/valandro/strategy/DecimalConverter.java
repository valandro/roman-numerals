package br.com.valandro.strategy;

import br.com.valandro.domain.RomanAlgarism;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import java.util.Optional;

public class DecimalConverter implements ConverterStrategy {

    private static final String ONLY_NUMBERS_REGEX = "\\d*";
    private static final Integer MAXIMUM_INTEGER_ALLOWED = 3999;
    private static final Integer LESS_THEN = 0;

    public Boolean verifyStrategy(final String value) {
        return Optional.ofNullable(value)
                       .map(v -> v.matches(ONLY_NUMBERS_REGEX))
                       .filter(valid -> valid)
                       .filter(number -> validateMaximumNumber(value))
                       .orElse(Boolean.FALSE);
    }


    /*
     *   Look for the greatest roman number divisible by a number with quotient greater than zero,
     *   increments N times the output and decrements this value from the number.
     */
    public String convert(final String value) {
        final RomanAlgarism[] romanAlgarisms = RomanAlgarism.values();
        double decimal = Double.valueOf(value);
        ArrayUtils.reverse(romanAlgarisms);
        StringBuilder roman = new StringBuilder();

        for (RomanAlgarism romanAlgarism : romanAlgarisms) {
            Double quotient = Math.floor(decimal / romanAlgarism.getNumber());
            roman.append(StringUtils.repeat(romanAlgarism.name(), quotient.intValue()));
            decimal -= quotient * romanAlgarism.getNumber();
        }

        return roman.toString();
    }

    private Boolean validateMaximumNumber(final String number) {
        return Integer.valueOf(number).compareTo(MAXIMUM_INTEGER_ALLOWED) <= LESS_THEN;
    }
}
