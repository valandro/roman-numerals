package br.com.valandro.strategy;

public interface ConverterStrategy {

    Boolean verifyStrategy(String value);

    String convert(String value);
}
