package br.com.valandro.strategy;

import br.com.valandro.domain.RomanAlgarism;
import java.util.Optional;

public class RomanConverter implements ConverterStrategy {
    private static final String ONLY_ROMAN_NUMBERS = "^(?=[MDCLXVI])M{0,3}(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})";

    public Boolean verifyStrategy(final String value) {
        return Optional.ofNullable(value)
                       .map(String::toUpperCase)
                       .map(v -> v.matches(ONLY_ROMAN_NUMBERS))
                       .orElse(Boolean.FALSE);
    }

    /*
     * Look for the roman numerals with 2 algarisms first (IV, IX, XL, XC, CD, CM), and if doesn't find, them look for
     * single algarism value.
     */
    public String convert(final String value) {
        Integer decimal = 0;
        final String upperCaseValue = value.toUpperCase();

        for (int index = 0; index < value.length(); index++) {
            int endIndex = (value.length() >= index + 2) ? index + 2 : index + 1;
            final String twoAlgarisms = upperCaseValue.substring(index, endIndex);
            if (RomanAlgarism.isValid(twoAlgarisms)) {
                decimal += RomanAlgarism.getNumber(twoAlgarisms);
                index++;
            } else {
                final String singleAlgarism = upperCaseValue.substring(index, index + 1);
                decimal += RomanAlgarism.getNumber(singleAlgarism);
            }
        }

        return String.valueOf(decimal);
    }
}
