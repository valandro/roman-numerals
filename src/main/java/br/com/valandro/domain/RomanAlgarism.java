package br.com.valandro.domain;

import java.util.Arrays;

public enum RomanAlgarism {

    I(1),
    IV(4),
    V(5),
    IX(9),
    X(10),
    XL(40),
    L(50),
    XC(90),
    C(100),
    CD(400),
    D(500),
    CM(900),
    M(1000);

    private Integer number;

    RomanAlgarism(final Integer number) {
        this.number = number;
    }

    public static RomanAlgarism[] getRepeatableAlgarisms() {
        return new RomanAlgarism[]{I, X, C, M};
    }

    public Integer getNumber() {
        return this.number;
    }

    public static Integer getNumber(final String value) {
        return  Arrays.stream(RomanAlgarism.values())
                      .filter(romanAlgarism -> romanAlgarism.name().equals(value))
                      .findFirst()
                      .map(RomanAlgarism::getNumber)
                      .orElse(0);
    }

    public static Boolean isValid(final String value) {
        return Arrays.stream(RomanAlgarism.values())
                     .anyMatch(romanAlgarism -> romanAlgarism.name().equals(value));
    }
}
